![Vergiss Es Nie Noten.pdf BETTER](https://3fc4ed44-3fbc-419a-97a1-a29742511391.selcdn.net/coub_storage/story/cw_image_for_sharing/8425f106039/d74f612c9120e98374627/1642249766_share_story.png)
 
# Vergiss Es Nie: A Beautiful Song by Paul Janz
 
Vergiss Es Nie (Never Forget) is a song by Paul Janz, a Canadian singer-songwriter and theologian. The song was released in 1999 as part of his album *High Strung*. It is a rock ballad that expresses the unconditional love of God for every person.
 
**Download Zip ✸ [https://www.google.com/url?q=https%3A%2F%2Fblltly.com%2F2uEkER&sa=D&sntz=1&usg=AOvVaw21zq2JSdxyNWOmL7RlYQQk](https://www.google.com/url?q=https%3A%2F%2Fblltly.com%2F2uEkER&sa=D&sntz=1&usg=AOvVaw21zq2JSdxyNWOmL7RlYQQk)**


 
The lyrics of the song are in German, but they can be translated as follows:

> Vergiss es nie: Dass du lebst, war keine eigene Idee,
 Und dass du atmest, kein Entschluss von dir.
 Vergiss es nie: Dass du lebst, war eines anderen Idee,
 Und dass du atmest, sein Geschenk an dich.
> 
> 
> Never forget: That you live, was not your own idea,
 And that you breathe, no decision of yours.
 Never forget: That you live, was someone else's idea,
 And that you breathe, his gift to you.

The song has a simple but catchy melody that can be played on various instruments. The sheet music for the song can be found online in PDF format. For example, you can download and print the sheet music for vocals (solo) from Musescore.com[^1^] [^2^]. You can also listen to the song and view other versions of the composition on the same website.
 
Vergiss Es Nie Noten Klavier,  Vergiss Es Nie Noten Gitarre,  Vergiss Es Nie Noten Download,  Vergiss Es Nie Noten Kostenlos,  Vergiss Es Nie Noten Chor,  Vergiss Es Nie Noten Flöte,  Vergiss Es Nie Noten Geige,  Vergiss Es Nie Noten Saxophon,  Vergiss Es Nie Noten Trompete,  Vergiss Es Nie Noten Querflöte,  Vergiss Es Nie Noten Akkordeon,  Vergiss Es Nie Noten Ukulele,  Vergiss Es Nie Noten Keyboard,  Vergiss Es Nie Noten Schlagzeug,  Vergiss Es Nie Noten Violine,  Vergiss Es Nie Noten Cello,  Vergiss Es Nie Noten Blockflöte,  Vergiss Es Nie Noten Klarinette,  Vergiss Es Nie Noten Orgel,  Vergiss Es Nie Noten Harfe,  Vergiss Es Nie Noten Oboe,  Vergiss Es Nie Noten Fagott,  Vergiss Es Nie Noten Horn,  Vergiss Es Nie Noten Posaune,  Vergiss Es Nie Noten Tuba,  Vergiss Es Nie Liedtext und Noten,  Vergiss Es Nie Lied mit Noten,  Vergiss Es Nie Lied zum Ausdrucken mit Noten,  Vergiss Es Nie Lied zum Anhören mit Noten,  Vergiss Es Nie Lied zum Mitsingen mit Noten,  Vergiss Es Nie Lied zum Spielen mit Noten,  Vergiss Es Nie Lied zum Lernen mit Noten,  Vergiss Es Nie Lied zum Üben mit Noten,  Vergiss Es Nie Lied zum Vorspielen mit Noten,  Vergiss Es Nie Lied zum Singen mit Noten,  Vergiss Es Nie Lied zum Tanzen mit Noten,  Vergiss Es Nie Lied zum Feiern mit Noten,  Vergiss Es Nie Lied zum Beten mit Noten,  Vergiss Es Nie Lied zum Meditieren mit Noten,  Vergiss Es Nie Lied zum Entspannen mit Noten,  Wie spiele ich Vergiss Es Nie auf dem Klavier?,  Wie spiele ich Vergiss Es Nie auf der Gitarre?,  Wie spiele ich Vergiss Es Nie auf der Flöte?,  Wie spiele ich Vergiss Es Nie auf der Geige?,  Wie spiele ich Vergiss Es Nie auf dem Saxophon?,  Wie spiele ich Vergiss Es Nie auf der Trompete?,  Wie spiele ich Vergiss Es Nie auf der Querflöte?,  Wie spiele ich Vergiss Es Nie auf dem Akkordeon?,  Wie spiele ich Vergiss Es Nie auf der Ukulele?,  Wie spiele ich Vergiss Es Nie auf dem Keyboard?
 
Vergiss Es Nie is a popular song for weddings, baptisms, and other occasions that celebrate life and love. It is also a song that can inspire hope and gratitude in times of difficulty and doubt. The message of the song is clear: You are loved by God, no matter what.
  
Paul Janz was born in 1951 in Three Hills, Alberta, Canada. He moved to Switzerland with his family when he was only four years old. He grew up in a Mennonite family and was influenced by gospel and choir music. He learned to play the trumpet and guitar and studied opera at the Basel Conservatory of Music[^1^].
 
In the 1970s, he formed a band called Deliverance with his brother, cousin, and friends. The band played a mix of gospel and rock music and recorded four albums. They were successful in Germany and had a hit with "Leaving LA" in 1979[^2^]. Janz left the band in 1980 and returned to Canada to pursue a solo career.
 
He signed with A&M Records in 1984 and released his first solo album, *High Strung*, in 1985. The album achieved gold status in Canada and featured the hit single "Go to Pieces". He won a Juno Award for most promising male vocalist that year[^2^]. His second album, *Electricity*, came out in 1987 and included the songs "One Night" and "Believe in Me". His third album, *Renegade Romantic*, was released in 1990 and contained the singles "Every Little Tear" and "I Won't Cry". His fourth and final album, *Trust*, was released in 1992 under a co-partnership with Attic Records[^1^].
  
After his music career, Janz pursued a different path: theology. He earned a PhD in philosophical theology from King's College London in 1998 and became a professor of theology at the same institution. He has written several books and articles on topics such as theological apologetics, God and reason, Christian thinking, and the relationship between philosophy and theology[^1^].
 
One of his main contributions to theology is his concept of the command of grace. In his book *The Command of Grace: A New Theological Apologetics*, he argues that God's grace is not a passive gift that humans can accept or reject, but a dynamic and creative power that commands human response. He writes: "Grace is not a thing that God gives to us; it is God's own self-communication to us. Grace is not something that we receive; it is something that we are commanded by"[^4^].
 
Janz challenges the traditional views of apologetics that try to prove God's existence or justify Christian beliefs by rational arguments. He proposes a new way of doing apologetics that is based on the living reality of God's grace and its transformative impact on human existence. He claims that Christian thinking is not a matter of defending doctrines or finding evidences, but a matter of responding faithfully and creatively to God's command of grace[^4^].
 63edc74c80
 
